{-# LANGUAGE OverloadedStrings, LambdaCase #-}

module Main (main) where

import qualified Data.ByteString as BS
import Control.Error.Util
import GHC.Data.Maybe
import qualified Data.List as L
import Control.Monad
import Control.Monad.Loops
import Control.Monad.Trans.Except
import Control.Monad.IO.Class
import qualified Data.Map as M
import qualified NetlistParse as NL
import Circuit
import TracePower
import ShowState
import Data.Tuple.Extra


main :: IO ()
main = do
    res <- runExceptT mainE
    case res of Left s -> putStrLn $ "Error: \"" ++ s ++ "\""
                _      -> putStrLn "Simulation complete"

mainE :: ExceptT String IO ()
mainE = do
    xmlbytes <- liftIO $ BS.readFile "Mastermind_netlist.xml"
    circ <- hoistEither $ do
                netlist <- NL.netlistParse xmlbytes
                NL.checkNetlist netlist
                deriveCircuit netlist
    let initState = initialCircuitState circ
    let fmt = formatForState initState
    liftIO $ putStrLn $ displayFormat fmt
    let step' cs = do
            -- Display states as we visit them. The simulation may never end
            liftIO $ unless (hasTrans cs) $ putStrLn $ showState fmt cs
            hoistEither $ step circ cs
    void $ iterateM step' initState

hasTrans :: Foldable f => f RelayState -> Bool
hasTrans = any (\s -> s == TransOn || s == TransOff)

step :: Circuit -> CircuitState -> Either String (Maybe CircuitState)
step circ circState =
    let live = tracePower circ circState Powered
        gnd  = tracePower circ circState Grounded
    in do
        checkBadConnection live gnd
        let inducedState = inducedCircuitState circ live gnd
        checkForStutter circState inducedState
        stateChanges <- sequence $ [startTransitions, completeTransition, clockIncrement] <*> pure circState <*> pure inducedState
        -- Use the first state change from this list that is applicable
        return $ firstJusts stateChanges

checkBadConnection :: [Connector] -> [Connector] -> Either String ()
checkBadConnection live gnd = do
    unless (null $ L.intersect live gnd) $ Left "Short"
    when (any ((== AMinus) . pad) live) $ Left "\"AMinus\" powered"
    when (any ((== APlus) . pad) gnd) $ Left "\"APlus\" grounded"

-- Check that a relay doesn't start transitioning and then get returned to its original state
checkForStutter :: CircuitState -> CircuitState -> Either String ()
checkForStutter cur ind =
    let ncs = nonClocks cur
        stutter p = case p of (TransOn, Off) -> True
                              (TransOff, On) -> True
                              _              -> False
    in when (any stutter (pairMaps ncs ind)) $ Left "Relay stuttered"

-- Start all induced transitions
startTransitions :: CircuitState -> CircuitState -> Either String (Maybe CircuitState)
startTransitions cur ind =
    let ncs = nonClocks cur
        trans p = case p of (Off, On) -> Just TransOn
                            (On, Off) -> Just TransOff
                            _         -> Nothing
        changes = M.mapMaybe trans $ pairMaps ncs ind
    -- Apply all such changes. If there are none then this transition is not applicable
    in return $ if M.null changes then Nothing
                                  else Just $ changes `M.union` cur

-- Complete a transition
completeTransition :: CircuitState -> CircuitState -> Either String (Maybe CircuitState)
completeTransition cur _ =
    let ncs = nonClocks cur
        comp s = case s of TransOn  -> Just On
                           TransOff -> Just Off
                           _        -> Nothing
        changes = M.mapMaybe comp ncs
    -- Apply one such change. If there are none then this transition is not applicable
    in return $ case M.toList changes of (r, rs):_ -> Just $ M.insert r rs cur
                                         _         -> Nothing


clockIncrement :: CircuitState -> CircuitState -> Either String (Maybe CircuitState)
clockIncrement cur _ = do
    let tick p = case p of (Off, Off) -> (On, Off)
                           (On, Off)  -> (On, On)
                           (On, On)   -> (Off, On)
                           (Off, On)  -> (Off, Off)
                           _          -> (Off, Off)
    let clk1 = Relay "Clk1"
    let clk2 = Relay "Clk2"
    clk1state <- cur !$ clk1
    clk2state <- cur !$ clk2
    let (n1, n2) = tick (clk1state, clk2state)
    let next = M.fromList [(clk1, n1), (clk2, n2)] `M.union` cur
    ss <- stopingState cur
    return $ if ss then Nothing else Just next

stopingState :: CircuitState -> Either String Bool
stopingState cur =
    let ss = [(Relay "ShftA4", Off), (Relay "ShftA5", On),
              (Relay "ShftB3", Off), (Relay "ShftB4", On)]
        test (r, s) = (== s) <$> cur !$ r
    in and <$> traverse test ss

isClock :: Part -> Bool
isClock p = case p of Relay "Clk1" -> True
                      Relay "Clk2" -> True
                      _            -> False

nonClocks :: M.Map Part a -> M.Map Part a
nonClocks = M.filterWithKey (\k _ -> not $ isClock k)

pairMaps :: Ord k => M.Map k a -> M.Map k b -> M.Map k (a, b)
pairMaps m n = M.mapWithKey (\k v -> (v, n M.! k)) m

iterateM :: Monad m => (t -> m (Maybe t)) -> t -> m [t]
iterateM f = unfoldrM (fmap (fmap dupe) . f)

(!$) :: CircuitState -> Part -> Either String RelayState
(!$) m x = note (show x ++ " not found in " ++ show [r | Relay r <- M.keys m]) $ M.lookup x m