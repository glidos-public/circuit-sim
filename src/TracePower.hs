module TracePower (
    PadState(..),
    RelayState(..),
    CircuitState,
    initialCircuitState,
    inducedCircuitState,
    tracePower
) where

import Data.Map as M
    ( Map, lookup, fromList, mapKeys, unionsWith, (!), empty )
import Data.List as L ( null, singleton, (\\), union )
import Data.Containers.ListUtils ( nubOrd )
import Control.Error ( mapMaybe )
import Circuit
    ( Circuit, Connector(..), Pin(..), Part(..), componentsOfcircuit )

data PadState = Powered | Grounded
data RelayState = Off | On | TransOff | TransOn deriving (Show, Eq, Ord)

type CircuitState = M.Map Part RelayState

initialCircuitState :: Circuit -> CircuitState
initialCircuitState circuit =
     let components = componentsOfcircuit circuit
         relays = [r | r@(Relay _) <- components]
     in M.fromList [(r, Off) | r <- relays]

inducedCircuitState :: Circuit -> [Connector] -> [Connector] -> CircuitState
inducedCircuitState circ live gnd =
     let components = componentsOfcircuit circ
         drivenState r =  if (Connector APlus r `elem` live) && (Connector AMinus r `elem` gnd) then On else Off
     in M.fromList [(r, drivenState r) | r@(Relay _) <- components]

source :: PadState -> Connector
source padState = case padState of Powered -> Connector VPlus Power
                                   Grounded -> Connector VMinus Ground

tracePower :: Circuit -> CircuitState -> PadState -> [Connector]
tracePower circ circState padState =
     let netMap = mapFromNetlist circ
         compMap = componentConnectMap circ circState padState
         fullMap = joinConnectMaps [compMap, netMap]
     in flood fullMap [source padState]

nameConnectMap :: Part -> Map Pin [Pin] -> Map Connector [Connector]
nameConnectMap nm =
     let mkConnector pin = Connector pin nm
     in (fmap.fmap) mkConnector . M.mapKeys mkConnector

mapFromPairs :: Ord a => [(a, a)] -> Map a [a]
mapFromPairs = fmap L.singleton . M.fromList

mapFromNetlist :: Ord a => [[a]] -> Map a [a]
mapFromNetlist s =
     let mapFromNet n = M.fromList [(e, n L.\\ [e]) | e <- n]
     in joinConnectMaps $ mapFromNet <$> s

joinConnectMaps :: Ord k => [M.Map k [v]] -> M.Map k [v]
joinConnectMaps = M.unionsWith (++)

componentConnectMap :: Circuit -> CircuitState -> PadState -> Map Connector [Connector]
componentConnectMap circuit circuitState' padState =
     let components = componentsOfcircuit circuit
         ccmap comp = nameConnectMap comp $
          case comp of
               Relay _ -> mapFromNetlist $ case circuitState' M.! comp of
                    Off -> [[COM0, NC0], [COM1, NC1]]
                    On  -> [[COM0, NO0], [COM1, NO1]]
                    _   -> []
               Diode _ -> mapFromPairs $ case padState of
                    Powered  -> [(Anode, Cathode)]
                    Grounded -> [(Cathode, Anode)]
               _     -> M.empty
     in joinConnectMaps $ ccmap <$> components

flood :: Ord a => Map a [a] -> [a] -> [a]
flood next seeds =
     let step (visited, edge) =
          let visited' = nubOrd $ visited `L.union` edge
              edge' = nubOrd (mconcat $ mapMaybe (`M.lookup` next) edge) L.\\ visited'
          in (visited', edge')
     in fst $ until (L.null . snd) step ([], seeds)
