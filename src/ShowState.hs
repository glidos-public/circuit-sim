{-# LANGUAGE OverloadedStrings #-}
module ShowState
(
    formatForState,
    displayFormat,
    showState
) where

import GHC.Exts ( groupWith )
import Data.List ( intercalate )
import qualified Data.Map as M
import Text.Read ( readMaybe )
import Text.Regex.Applicative
    ( Alternative(many), anySym, few, match, psym, RE )
import Circuit ( Part(Relay) )
import TracePower ( CircuitState, RelayState(On) )
import Data.Char ( isDigit )
import Control.Error ( catMaybes, mapMaybe )


data RelayName = RelayName String (Maybe Int)

instance Show RelayName where
    show (RelayName s i) = s ++ maybe "" show i

type Format = [[String]]

regexRelayName :: RE Char RelayName
regexRelayName = RelayName <$> few anySym <*> (readMaybe <$> many (psym isDigit))

matchRelayName :: [Char] -> Maybe RelayName
matchRelayName = match regexRelayName

formatForState :: CircuitState -> Format
formatForState cs =
    let rnames = catMaybes [ matchRelayName s | Relay s <- M.keys cs]
        groups = groupWith (\(RelayName s _) -> s) rnames
    in (fmap.fmap) show groups

displayFormat :: Format -> String
displayFormat fmt =
    let disp n = (\(RelayName s _) -> s) <$> matchRelayName n
    in intercalate "|" $ head . mapMaybe disp <$> fmt

showState :: Format -> CircuitState -> String
showState fmt cs =
    let disp s = case cs M.! Relay s of On -> "X"
                                        _  -> " "
    in "{" ++ intercalate "|" (foldMap disp <$> fmt) ++ "}"
