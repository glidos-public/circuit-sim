{-# LANGUAGE OverloadedStrings #-}
module NetlistParse
(
    Connector(..),
    Part(..),
    Netlist,
    netlistParse,
    checkNetlist
) where

import GHC.Exts ( groupWith )
import Data.ByteString.UTF8 as BS ( ByteString, toString )
import Data.Either.Extra ( mapLeft )
import Data.List.Extra ( sort, nubOrd )
import Control.Monad ( (<=<), unless, when )
import Xeno.DOM
    ( Node, Content(Element), parse, name, contents, attributes )

data Part = Part {title :: String, label :: String} deriving (Show, Eq, Ord)
data Connector = Connector {cName :: String, cPart :: Part} deriving (Show, Eq, Ord)
type Net = [Connector]
type Netlist = [Net]

checkNetlist :: Netlist -> Either String ()
checkNetlist netlist =
    let parts = sort $ nubOrd $ cPart <$> mconcat netlist
    in do
        when (any (null . label) parts) $ Left "Connector with null label"
        unless (all ((== 1) . length) $ groupWith label parts) $ Left "Component labels not unique"

netlistParse :: BS.ByteString -> Either String Netlist
netlistParse = netlistNodeParse <=< mapLeft show . parse

netlistNodeParse :: Node -> Either String Netlist
netlistNodeParse n = case name n of
    "netlist" -> traverse netNodeParse [en | Element en <- contents n]
    _         -> Left "Outer node not \"netlist\""

netNodeParse :: Node -> Either String Net
netNodeParse n = case name n of
    "net" -> nubOrd <$> traverse connectorNodeParse [en | Element en <- contents n]
    _     -> Left "Something other than a \"net\" in a \"netlist\""

connectorNodeParse :: Node -> Either String Connector
connectorNodeParse n = case name n of
    "connector" -> do
                     nm <- lookupAttr n "name"
                     parts <- traverse partNodeParse [en | Element en <- contents n]
                     case parts of [p]     -> return $ Connector nm p
                                   []      -> Left "Connector has no parts"
                                   _       -> Left "Connector has multiple parts"
    _           -> Left "Something other than a \"connector\" in a \"net\""

partNodeParse :: Node -> Either String Part
partNodeParse n = case name n of
    "part" -> Part <$> lookupAttr n "title" <*> lookupAttr n "label"
    _      -> Left "Something other than a \"part\" in a \"connector\""

lookupAttr :: Node -> BS.ByteString -> Either String String
lookupAttr n key = let vs = [v | (k,v) <- attributes n, k == key]
                   in case vs of [v] -> return $ BS.toString v
                                 []  -> Left $ "Lookup failed for attribute \"" ++ BS.toString key ++ "\""
                                 _   -> Left $ "Multiple values found for attribute \"" ++ BS.toString key ++ "\""
