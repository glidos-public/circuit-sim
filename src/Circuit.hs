{-# LANGUAGE LambdaCase #-}

module Circuit
(
    Part(..),
    Pin(..),
    Connector(..),
    Circuit,
    deriveCircuit,
    componentsOfcircuit
) where

import Control.Monad ( unless )
import Control.Error.Util ( note )
import Data.Foldable ( traverse_ )
import Text.Regex.Applicative
    ( Alternative((<|>), many), anySym, match, string, RE )
import qualified NetlistParse as NL
import Data.List.Extra ( nubOrd )

-- Instances of Power are always connected, so it is best not
-- to distinguish them by keeping their labels. There will be
-- a net that connects them. We will coalesce those instances
-- for efficiency. Likewise for ground.
data Part = Relay String
          | Diode String
          | Capacitor String
          | Resister String
          | Power
          | Ground deriving (Show, Eq, Ord)

data Pin = APlus | AMinus | COM0 | NO0 | NC0 | COM1 | NO1 | NC1
         | Anode | Cathode
         | CPin0 | CPin1
         | RPin0 | RPin1
         | VPlus | VMinus deriving (Show, Eq, Ord)


regexPart :: RE Char (String -> Part)
regexPart = (Relay <$ string "HFD3")
            <|> (Diode <$ string "Rectifier Diode")
            <|> (Capacitor <$ string "Ceramic Capacitor")
            <|> (Resister <$ (many anySym <> string "Resistor"))
            <|> (const Power <$ string "Power")
            <|> (const Ground <$ string "Ground")

regexPin :: RE Char Pin
regexPin = APlus <$ string "A+"
            <|> AMinus <$ string "A-"
            <|> COM0 <$ string "COM0"
            <|> NO0 <$ string "NO0"
            <|> NC0 <$ string "NC0"
            <|> COM1 <$ string "COM1"
            <|> NO1 <$ string "NO1"
            <|> NC1 <$ string "NC1"
            <|> Anode <$ string "anode"
            <|> Cathode <$ string "cathode"
            <|> CPin0 <$ string "0"
            <|> CPin1 <$ string "1"
            <|> RPin0 <$ string "Pin 0"
            <|> RPin1 <$ string "Pin 1"
            <|> VPlus <$ string "V+"
            <|> VMinus <$ string "GND"


data Connector = Connector {pad :: Pin, component :: Part} deriving (Show, Eq, Ord)

type Circuit = [[Connector]]

hasPins :: Part -> [Pin]
hasPins = \case Relay _     -> [APlus, AMinus, COM0, NO0, NC0, COM1, NO1, NC1]
                Diode _     -> [Anode, Cathode]
                Capacitor _ -> [CPin0, CPin1]
                Resister _  -> [RPin0, RPin1]
                Power       -> [VPlus]
                Ground      -> [VMinus]

matchPart :: [Char] -> Either [Char] (String -> Part)
matchPart s = note ("Unrecognised part: " ++ s) $ match regexPart s

matchPin :: [Char] -> Either [Char] Pin
matchPin s = note ("Unrecognised pin: " ++ s) $ match regexPin s

deriveCircuit :: NL.Netlist -> Either String Circuit
deriveCircuit netlist = do
    circ <- (traverse.traverse) nlConnectorToConnector netlist
    -- Some nets will have multiple instances of the power connector
    -- or the ground connector. They are best coalesced for efficiency.
    let nrcirc = nubOrd <$> circ
    traverse_ checkPad $ mconcat nrcirc
    return nrcirc

componentsOfcircuit :: Circuit -> [Part]
componentsOfcircuit circ = [p | Connector _ p <- mconcat circ]

nlPartToPart :: NL.Part -> Either String Part
nlPartToPart (NL.Part title' label') = matchPart title' <*> pure label'

nlConnectorToConnector :: NL.Connector -> Either String Connector
nlConnectorToConnector (NL.Connector cName' cPart') = Connector <$> matchPin cName' <*> nlPartToPart cPart'

checkPad :: Connector -> Either String ()
checkPad (Connector pin part) =
        let pins = hasPins part
        in unless (pin `elem` pins) $ Left $ "Component \"" ++ show part ++ "\" has pin \"" ++ show pin ++ "\""
